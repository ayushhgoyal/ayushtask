package com.punchh.punchh_ayush;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.punchh.punchh_ayush.custom.CircleLayout;
import com.punchh.punchh_ayush.custom.CircleProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.normalWithRange)
    CircleLayout circleLayout;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.count_tv)
    TextView countTv;
    @BindView(R.id.circular_progress_bar)
    CircleProgressBar circleProgressBar;

    public ArrayList<ImageView> getImageViews() {
        if (imageViews == null) {
            imageViews = new ArrayList<>();
            for (int i = 0; i < circleLayout.getChildCount(); i++) {
                imageViews.add((ImageView) circleLayout.getChildAt(i));
            }

        }
        return imageViews;
    }

    ArrayList<ImageView> imageViews;

    public int getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(int visitCount) {
        this.visitCount = visitCount;
        updateCountTv(visitCount);
    }

    private void updateCountTv(int visitCount) {
        countTv.setText(visitCount + "/" + getImageViews().size());
    }

    int visitCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setNumberOfVisits(getVisitCount());
    }

    public void onEarnButtonClick(View v) {
        setNumberOfVisits(getVisitCount() + 1);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(getString(R.string.count), getVisitCount());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setVisitCount(savedInstanceState.getInt(getString(R.string.count)));
        setNumberOfVisits(getVisitCount());

    }

    /**
     * sets bubbles filled according to count
     *
     * @param count
     */
    private void setNumberOfVisits(int count) {
        if (count <= getImageViews().size()) {
            for (int i = 0; i < count; i++) {
                getImageViews().get(i).setImageResource(R.drawable.filled_circle);
            }
            for (int j = count; j < getImageViews().size(); j++) {
                getImageViews().get(j).setImageResource(R.drawable.empty_circle);
            }

            // reset count when target is reached
            if (count >= getImageViews().size()) {

                Toast.makeText(this, "You have earned a haircut!", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setNumberOfVisits(0);
                    }
                }, 500);
            }
            setVisitCount(count);
            setCircularProgress(count);

        }
    }

    private void setCircularProgress(int count) {
        if (count > 0) {
            float stepper = 5f;
            float progress = stepper * count;
            circleProgressBar.setProgress(progress);
        } else {
            circleProgressBar.setProgress(0);
        }

    }
}
